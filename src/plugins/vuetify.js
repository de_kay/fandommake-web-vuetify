/**
 * plugins/vuetify.js
 *
 * Framework documentation: https://vuetifyjs.com`
 */

// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Composables
import { createVuetify } from 'vuetify'

// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
export default createVuetify({
  theme: {
    defaultTheme: 'darky',
    themes: {
      darky: {
        dark: true,
        colors: {
          primary: '#2A2A30',
          secondary: '#15181d',
          background: '#000',
          subBackground: '#2A2A2A',
          lighty: '#FFF',
          'on-lighty': '#505050',
        },
      },
      lighty: {
        light: true,
        colors: {
          primary: '#2A2A30',
          secondary: '#15181d',
          background: '#FFF',
          subBackground: '#2A2A2A',
          'on-lighty': '#505050',
        },
      },
    },
  }
})
