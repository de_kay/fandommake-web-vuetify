const numberComma = (num) => {
    return String(num).replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

const splitDate = (dateTime) => {
    return dateTime.split(' ')[0];
}

const dateFormatYMD = (dateValue) => {
    const date = new Date(dateValue);
    let m = date.getMonth()+1;
    if(m < 10) {
        m = '0'+m;
    }
    let d = date.getDate();
    if(d < 10) {
        d = '0'+d;
    }
    return date.getFullYear() + '-' + m + '-' + d;
}

const getCurrentTime = (t) => {
    const date = t ? new Date(t) : new Date();
    let h = date.getHours();
    if(h < 10) h = '0' + h;
    let m = date.getMinutes();
    if(m < 10) m = '0' + m;
    let s = date.getSeconds();
    if(s < 10) s = '0' + s;
    return h + ':' + m + ':' + s + '.' + '001';
}

const getTimezone = () => {
    const curTime = new Date().toString();
    const curTimeArr = curTime.split(' ');
    const curTimezone = curTimeArr[5] + ' ' + Intl.DateTimeFormat().resolvedOptions().timeZone;
    return curTimezone;
}

const replaceReferenceCategoryToTxt = (category) => {
    if(category === 'BANNER') {
        return '배너 광고';
    } else if(category === 'LIVE') {
        return '라이브 방송';
    } else if(category === 'CAST') {
        return '출연 섭외';
    } else if(category === 'NEWS') {
        return '뉴스';
    } else if(category === 'CBT&QA') {
        return 'CBT&QA';
    } else {
        return 'UNKNOWN';
    }
    
}

export { numberComma, splitDate, dateFormatYMD, getCurrentTime, getTimezone, replaceReferenceCategoryToTxt };