import { createRouter, createWebHistory } from 'vue-router'
import { useAuthStore } from "@/store/auth";

const routes = [
  {
    path: '/',
    component: () => import('@/layouts/Default.vue'),
    children: [
      {
        path: '/',
        name: 'Home',
        component: () => import('@/components/home/Home.vue'),
      },
      {
        path: '/creator',
        name: 'Creator',
        component: () => import('@/components/creator/Creator.vue'),
      },
      {
        path: '/reference',
        name: 'Business',
        component: () => import('@/components/business/Business.vue'),
      },
      {
        path: '/contact',
        name: 'Contact',
        component: () => import('@/components/contact/Contact.vue'),
      },
    ]
  },
  {
    path: '/b/signin',
    name: 'SignIn',
    component: () => import('@/components/admin/SignIn.vue'),
  },
  {
    path: '/b',
    component: () => import('@/layouts/admin/Default.vue'),
    children: [
      {
        path: '/b/admin',
        name: 'Admin',
        component: () => import('@/components/admin/Admin.vue'),
        meta: { roles: [ 'ADMIN', 'SUBADMIN' ] },
      },
      {
        path: '/b/admin/creatorsData',
        name: 'CreatorsData',
        component: () => import('@/components/admin/CreatorsData.vue'),
        meta: { roles: [ 'ADMIN', 'SUBADMIN' ] },
      },
      {
        path: '/b/admin/contactData',
        name: 'ContactData',
        component: () => import('@/components/admin/ContactData.vue'),
        meta: { roles: [ 'ADMIN', 'SUBADMIN' ] },
      },
      {
        path: '/b/admin/footerData',
        name: 'FooterData',
        component: () => import('@/components/admin/FooterData.vue'),
        meta: { roles: [ 'ADMIN', 'SUBADMIN' ] },
      },
      {
        path: '/b/admin/referenceData',
        name: 'ReferenceData',
        component: () => import('@/components/admin/ReferenceData.vue'),
        meta: { roles: [ 'ADMIN', 'SUBADMIN' ] },
      },
      {
        path: '/b/admin/excelDownload',
        name: 'ExcelDownload',
        component: () => import('@/components/admin/ExcelDownload.vue'),
        meta: { roles: [ 'ADMIN', 'SUBADMIN' ] },
      },
    ]
  },
  {
    path: '/r',
    component: () => import('@/layouts/reference/Default.vue'),
    children: [
      {
        path: '/r/reference',
        name: 'Reference',
        component: () => import('@/components/reference/Reference.vue'),
      },
      {
        path: '/r/reference/:refNo',
        name: 'ReferencePost',
        component: () => import('@/components/reference/ReferencePost.vue'),
        props: true
      },
    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior() {
    window.scrollTo(0,0);
  }
})

router.beforeEach((to, from, next) => { // 페이지를 이동하기 전에 호출되는 함수
  const authStore = useAuthStore();
  if ((to.path.substr(0, 3) === "/b/" && to.name !== "SignIn") && !authStore.isLoggedIn) {
    // login으로 가고 있지 않고 로그인되어 있지 않으면 /login으로 redirect
    // 로그인하지 않은 상태에서 /를 요청하는 경우 (프로젝트가 처음 실행될 때)
    router.replace({
      name: 'SignIn',
      state: {
        target : to.name
      }
    })
  } else if ((to.name === "SignIn" || to.name === "SignUp" || to.name === "FindPassword") && authStore.isLoggedIn) {
    // login으로 가고 있고 로그인되어 있으면 /으로 redirect
    // 로그인한 상태에서 /login을 요청하는 경우
    next("/b/admin");
  } else {
    const roleStatus = !authStore.userDetail ? '' : authStore.userDetail.userType;
    if (to.meta.roles && !to.meta.roles.includes(roleStatus)) {
      if(authStore.userDetail.userType === 'ADMIN' || authStore.userDetail.userType === 'SUBADMIN') {
        next("/b/admin");
      } else {
        alert('접근 권한이 없습니다.');
        next(from);
      }
    } else {
      next();
    }
  }
});

export default router
