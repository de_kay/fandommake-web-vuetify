// Utilities
import { defineStore } from 'pinia'

export const useAppStore = defineStore('app', {
  state: () => ({
    showMenuModel : {},
    alertModel: {
      isAlert: false,
      alertType: '',
      alertText: ''
    }
  }),
  getters: {},
  actions: {},
})