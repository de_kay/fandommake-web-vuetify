// Utilities
import { defineStore } from 'pinia'
import { useSessionStorage } from '@vueuse/core';

export const useAuthStore = defineStore('auth', {
    state: () => ({
        userDetail: null,
        token: null,
        userUuid: null,
        info: useSessionStorage('userDetail', { userDetail: null, token: null } )
    }),
    getters: {
        isLoggedIn: (state) => {
            return state.userDetail != null;
        },
    },
    actions: {},
    persist: {
        enabled: true,
        strategies: [{ storage: localStorage }],
    },
})
